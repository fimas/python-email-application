A simple Python program for sending emails from various email accounts.

The program requires Python 3, Python3-tk, and Python3-sqlite. 
The program produces error dialogs if you haven't added any accounts to the database.

You can add and manage email accounts easily from the top menu bar.

![Screenshot](https://cloud.legohouse.nu/s/EfeX2FdKHXngYm3/preview)