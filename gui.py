import tkinter as tk
from accounts import Accounts
from accounts import Add_Accounts
from accounts import Edit_Account
from emailer import Emailer

class GUI:
    def __init__(self, master):
        self.master = master
        self.mainframe = tk.Frame(self.master)
        self.accounts = Accounts()
        self.account_adder = Add_Accounts(self.master, self.accounts)
        self.build_layout()
        self.build_menu()

        if self.accounts.get_account() is None:
            self.master.title("No accounts available")
        else:
            self.master.title("Current account: " + self.accounts.get_account()['address'])
        self.mainframe.grid(row=0, column=0, padx=5, pady=(0,10))

    def build_layout(self):
        self.add_to_field()
        self.add_subject_field()
        self.add_message_box()
        self.add_send_button()

    def build_menu(self):
        self.menubar = tk.Menu(self.master)
        
        self.create_file_menu()
        self.create_edit_menu()
        self.create_help_menu()

        self.master.config(menu=self.menubar)

    def create_file_menu(self):
        self.filemenu = tk.Menu(self.menubar, tearoff=0)

        self.filemenu.add_command(label="Add Account", command=self.account_adder.display_account_adder)

        if self.accounts.get_account() is None:
            self.filemenu.add_command(label="Edit Account", command=self.menu_action_none, state="disabled")
            self.filemenu.add_command(label="Delete Account", command=self.menu_action_none, state="disabled")
            self.filemenu.add_command(label="Select Account", command=self.menu_action_none, state="disabled")
        else:
            self.filemenu.add_command(label="Edit Account", command=self.edit_account)
            self.filemenu.add_command(label="Delete Account", command=self.delete_account)
            self.create_accounts_menu()
        
        self.menubar.add_cascade(label="File", menu=self.filemenu)

    def create_accounts_menu(self):
        self.accountmenu = tk.Menu(self.filemenu, tearoff=0)

        for account in self.accounts.get_all_accounts():
            self.accountmenu.add_command(label=account['address'], command=lambda account_id=account['id']: self.update_account(account_id))
        
        self.filemenu.add_cascade(label="Select Account", menu=self.accountmenu)

    def print_account_info(self):
        print(self.accounts.get_account())
        
    def create_edit_menu(self):
        pass

    def create_help_menu(self):
        pass
    
    # Menu Actions
    # This function is guaranteed to only contain pass
    def menu_action_none(self):
        pass

    def update_account(self, account_id):
        self.accounts.set_account_id(account_id)
        self.master.title("Current account: " + self.accounts.get_account()['address'])

    def edit_account(self):
        account_editor = Edit_Account(self.accounts)
        account_editor.display_account_editor()

    def delete_account(self):
        msg_box = tk.messagebox.askquestion("Delete Account", "Are you sure you want to delete the account " + self.accounts.get_account()['address'] + "? The application will need to be restarted.", icon='warning')

        if msg_box == 'yes':
            self.accounts.delete_account()
            self.master.destroy()
            
    # GUI Definitions
    def add_to_field(self):
        self.to_label = tk.Label(self.mainframe, text="To")
        self.to_entry = tk.Entry(self.mainframe, width=107)

        self.to_label.grid(row=0, column=0, sticky=tk.NW, padx=5)
        self.to_entry.grid(row=1, column=0, sticky=tk.NW, padx=5)

    def add_subject_field(self):
        self.subject_label = tk.Label(self.mainframe, text="Subject")
        self.subject_entry = tk.Entry(self.mainframe, width=107)

        self.subject_label.grid(row=2, column=0, sticky=tk.NW, padx=5, pady=(10,0))
        self.subject_entry.grid(row=3, column=0, sticky=tk.NW, padx=5)

    def add_message_box(self):
        self.message_label = tk.Label(self.mainframe, text="Message")
        self.message_text = tk.Text(self.mainframe)

        self.message_label.grid(row=4, column=0, sticky=tk.NW, padx=5, pady=(10,0))
        self.message_text.grid(row=5, column=0, sticky=tk.NW, padx=5)

    def add_send_button(self):
        self.send_button = tk.Button(self.mainframe, text="Send", command=self.send_email)

        self.send_button.grid(row=6, column=0, sticky=tk.NW, padx=5, pady=(10,5))

    # GUI Actions
    def clear_fields(self):
        self.to_entry.delete(0, tk.END)
        self.subject_entry.delete(0, tk.END)
        self.message_text.delete(1.0, tk.END)

    def send_email(self):
        emailer = Emailer(self.accounts.get_account())
        to = self.to_entry.get()
        subject = self.subject_entry.get()
        message = self.message_text.get(1.0, tk.END)

        emailer.construct_email(to, subject, message)
        emailer.send_email()

        self.clear_fields()
