import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from tkinter import messagebox
from accounts import Accounts

class Emailer:
    def __init__(self, account):
        self.account = account
        self.message = MIMEMultipart()

    def construct_email(self, to, subject, message):
        self.set_headers(to, subject)
        self.set_body(message + "\n\n" + self.account['signature'])

    def set_headers(self, to, subject):
        self.message['From'] = self.account['address']
        self.message['To'] = to
        self.message['Subject'] = subject

    def set_body(self, message):
        self.message.attach(MIMEText(message, 'plain'))

    def send_email(self):
        try:
            text = self.message.as_string()
            
            with smtplib.SMTP_SSL(self.account['smtp'], int(self.account['port'])) as session:
                #session.starttls()
                session.ehlo_or_helo_if_needed()
                session.login(self.account['address'], self.account['password'])
                session.sendmail(self.account['address'], self.message['To'], text)
                
            messagebox.showinfo("Message Sent!", "Your message was successfully sent.")
        except smtplib.SMTPAuthenticationError as e1:
            messagebox.showerror("SMTP Authentication error", e1)
        except smtplib.SMTPConnectError as e2:
            messagebox.showerror("SMTP Connection error", e2)
        except smtplib.SMTPDataError as e3:
            messagebox.showerror("SMTP Data error", e3)
        except smtplib.SMTPException as e4:
            messagebox.showerror("SMTP error", e4)
        except smtplib.SMTPHeloError as e5:
            messagebox.showerror("SMTP Helo error", e5)
        except smtplib.SMTPNotSupportedError as e6:
            messagebox.showerror("SMTP Not Supported error", e6)
        except smtplib.SMTPRecipientsRefused as e7:
            messagebox.showerror("SMTP Recipients Refused error", e7)
        except smtplib.SMTPResponseException as e8:
            messagebox.showerror("SMTP Response error", e8)
        except smtplib.SMTPSenderRefused as e9:
            messagebox.showerror("SMTP Sender Refused error", e9)
        except smtplib.SMTPServerDisconnected as e10:
            messagebox.showerror("SMTP Server Disconnected", e10)
        except:
            messagebox.showerror("Couldn't send email", "An undefined error occured.")
