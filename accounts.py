import tkinter as tk
from tkinter import messagebox
import sqlite3
from sqlite3 import Error

class Accounts:
    def __init__(self):
        self.conn = sqlite3.connect("./email_accounts.db")
        self.create_account_table()
        self.account_id = self.get_lowest_id()

    def create_account_table(self):
        create_table_sql = """CREATE TABLE IF NOT EXISTS email_accounts (
                            id INTEGER PRIMARY KEY,
                            address TEXT NOT NULL,
                            password TEXT NOT NULL,
                            smtp TEXT NOT NULL,
                            port TEXT NOT NULL,
                            signature TEXT);"""

        try:
            cur = self.conn.cursor()
            cur.execute(create_table_sql)
        except Error as e:
            messagebox.showerror("Error", e)

    def get_all_accounts(self):
        try:
            cur = self.conn.cursor()
            cur.execute("SELECT * FROM email_accounts")
            labels = ['id', 'address', 'password', 'smtp', 'port', 'signature']
            return [dict(zip(labels, acc)) for acc in cur.fetchall()]
        except Error as e:
            messagebox.showerror("Error", e)
        except TypeError as e1:
            messagebox.showinfo("Setup", "The database is being initialized.\n\nIf you see this message again after restarting the application, please verify you have the SQLite Python extention installed.")

    def get_account(self):
        try:
            cur = self.conn.cursor()
            cur.execute("SELECT * FROM email_accounts WHERE id=?", (self.account_id,))
            labels = ['id', 'address', 'password', 'smtp', 'port', 'signature']
            return dict(zip(labels, cur.fetchone()))
        except Error as e:
            messagebox.showerror("Error", e)
        except TypeError as e1:
            messagebox.showinfo("Setup", "The database is being initialized.\n\nIf you see this message again after restarting the application, please verify you have the SQLite Python extention installed.")

    def add_account(self, address, password, smtp, port, signature=""):
        cur = self.conn.cursor()
        cur.execute("INSERT INTO email_accounts(address, password, smtp, port, signature) VALUES(?,?,?,?,?)", (address, password, smtp, port, signature))
        self.conn.commit()

    def update_account(self, address, password, smtp, port, signature=""):
        cur = self.conn.cursor()
        cur.execute("UPDATE email_accounts SET address=?, password=?, smtp=?, port=?, signature=? WHERE id=?", (address, password, smtp, port, signature, self.account_id))
        self.conn.commit()

    def delete_account(self):
        cur = self.conn.cursor()
        cur.execute("DELETE FROM email_accounts WHERE id=?", (self.account_id,))
        self.account_id = self.get_lowest_id()

    def get_lowest_id(self):
        cur = self.conn.cursor()
        cur.execute("SELECT MIN(id) FROM email_accounts")
        return cur.fetchone()[0]

    def set_account_id(self, account_id):
        self.account_id = account_id

class Add_Accounts:
    def __init__(self, master, accounts):
        self.master = master
        self.accounts = accounts

    def display_account_adder(self):
        self.root = tk.Tk()
        #self.root.geometry("500x700")
        self.root.title("Add Account")
        self.build_layout()
        self.root.mainloop()

    def build_layout(self):
        self.mainframe = tk.Frame(self.root)
        self.add_address_entry()
        self.add_password_entry()
        self.add_smtp_entry()
        self.add_port_entry()
        self.add_signature_text()
        self.add_save_button()
        self.mainframe.pack(padx=5, anchor=tk.NW, fill="x", pady=(0,10))

    def add_address_entry(self):
        self.address_label = tk.Label(self.mainframe, text="Address")
        self.address_entry = tk.Entry(self.mainframe)

        self.address_label.pack(side=tk.TOP, anchor=tk.NW, padx=5)
        self.address_entry.pack(side=tk.TOP, anchor=tk.NW, fill='x', padx=5)

    def add_password_entry(self):
        self.password_label = tk.Label(self.mainframe, text="Password")
        self.password_entry = tk.Entry(self.mainframe)

        self.password_label.pack(side=tk.TOP, anchor=tk.NW, padx=5, pady=(10,0))
        self.password_entry.pack(side=tk.TOP, anchor=tk.NW, fill='x', padx=5)

    def add_smtp_entry(self):
        self.smtp_label = tk.Label(self.mainframe, text="SMTP Server")
        self.smtp_entry = tk.Entry(self.mainframe)

        self.smtp_label.pack(side=tk.TOP, anchor=tk.NW, padx=5, pady=(10,0))
        self.smtp_entry.pack(side=tk.TOP, anchor=tk.NW, fill='x', padx=5)

    def add_port_entry(self):
        self.port_label = tk.Label(self.mainframe, text="Port Number")
        self.port_entry = tk.Entry(self.mainframe)

        self.port_label.pack(side=tk.TOP, anchor=tk.NW, padx=5, pady=(10,0))
        self.port_entry.pack(side=tk.TOP, anchor=tk.NW, fill='x', padx=5)

    def add_signature_text(self):
        self.signature_label = tk.Label(self.mainframe, text="Email Signature")
        self.signature_text = tk.Text(self.mainframe)

        self.signature_label.pack(side=tk.TOP, anchor=tk.NW, padx=5, pady=(10,0))
        self.signature_text.pack(side=tk.TOP, anchor=tk.NW, padx=5)

    def add_save_button(self):
        self.save_button = tk.Button(self.mainframe, text="Add Account", command=self.add_account)

        self.save_button.pack(side=tk.TOP, anchor=tk.NW, padx=5, pady=(10,5))

    def add_account(self):
        self.accounts.add_account(self.address_entry.get(), self.password_entry.get(), self.smtp_entry.get(), self.port_entry.get(), self.signature_text.get(1.0, tk.END))
        self.root.destroy()

class List_Accounts:
    pass

class Edit_Account:
    def __init__(self, accounts):
        self.accounts = accounts
        self.account = self.accounts.get_account()

    def display_account_editor(self):
        self.top = tk.Tk()
        self.top.title("Account Editor")
        self.mainframe = tk.Frame(self.top)
        self.build_layout()
        self.mainframe.grid(row=0, column=0, padx=5)
        self.top.mainloop()

    def build_layout(self):
        self.edit_address_entry()
        self.edit_password_entry()
        self.edit_smtp_entry()
        self.edit_port_entry()
        self.edit_signature_text()
        self.edit_save_button()

    def edit_address_entry(self):
        self.address_label = tk.Label(self.mainframe, text="Address")
        self.address_entry = tk.Entry(self.mainframe, width=80)

        self.address_label.grid(row=0, column=0, sticky=tk.NW, padx=5)
        self.address_entry.grid(row=1, column=0, sticky=tk.NW, padx=5)

        self.address_entry.insert(tk.END, self.account['address'])

    def edit_password_entry(self):
        self.password_label = tk.Label(self.mainframe, text="Password")
        self.password_entry = tk.Entry(self.mainframe, width=80)

        self.password_label.grid(row=2, column=0, sticky=tk.NW, padx=5, pady=(10,0))
        self.password_entry.grid(row=3, column=0, sticky=tk.NW, padx=5)

        self.password_entry.insert(tk.END, self.account['password'])

    def edit_smtp_entry(self):
        self.smtp_label = tk.Label(self.mainframe, text="SMTP")
        self.smtp_entry = tk.Entry(self.mainframe, width=80)

        self.smtp_label.grid(row=4, column=0, sticky=tk.NW, padx=5, pady=(10,0))
        self.smtp_entry.grid(row=5, column=0, sticky=tk.NW, padx=5)

        self.smtp_entry.insert(tk.END, self.account['smtp'])

    def edit_port_entry(self):
        self.port_label = tk.Label(self.mainframe, text="Port")
        self.port_entry = tk.Entry(self.mainframe, width=80)

        self.port_label.grid(row=6, column=0, sticky=tk.NW, padx=5, pady=(10,0))
        self.port_entry.grid(row=7, column=0, sticky=tk.NW, padx=5)

        self.port_entry.insert(tk.END, self.account['port'])

    def edit_signature_text(self):
        self.signature_label = tk.Label(self.mainframe, text="Email Signature")
        self.signature_text = tk.Text(self.mainframe, width=80)

        self.signature_label.grid(row=8, column=0, sticky=tk.NW, padx=5, pady=(10,0))
        self.signature_text.grid(row=9, column=0, sticky=tk.NW, padx=5)

        self.signature_text.insert(1.0, self.account['signature'])

    def edit_save_button(self):
        self.save_button = tk.Button(self.mainframe, text="Save", command=self.update_account)

        self.save_button.grid(row=10, column=0, sticky=tk.NW, padx=5, pady=(10,5))

    def update_account(self):
        self.accounts.update_account(self.address_entry.get(), self.password_entry.get(), self.smtp_entry.get(), self.port_entry.get(), self.signature_text.get(1.0, tk.END))
        self.top.destroy()
        
